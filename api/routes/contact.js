module.exports = (app) => {
    const Contact = require('../controllers/contact');

    app.route('/contacts')
        .get(Contact.list)
        .post((req, res) => Contact.create(req, res));
    
    app.route('/contacts/:id')
        .get(Contact.find)
        .put((req, res) => Contact.update(req, res))
        .delete((req, res) => Contact.remove(req, res));
};