module.exports = (app) => {
    const User = require('../controllers/user');

    app.route('/users')
        .post((req, res) => User.create(req, res));
    
    app.route('/users/authenticate')
        .post((req, res) => User.authenticate(req, res));
}