const mongoose = require('mongoose');
const Contact = mongoose.model('Contact');

exports.list = (req, res) => {
    Contact.find({}, (err, contacts) => {
        if (err) res.send(err);
        if (contacts) res.json(contacts);
    });
};

exports.create = (req, res) => {
    Contact.create(req.body, (err, contact) => {
        if (err) res.send(err);
        if (contact) res.json(contact);
    });
};

exports.find = (req, res) => {
    Contact.findById(req.params.id, (err, contact) => {
        if (err) res.send(err);
        if (contact) res.json(contact);
        if (!contact) res.send(404);
    });
};

exports.remove = (req, res) => {
    Contact.findByIdAndRemove(req.params.id, (err, deleted) => {
        if (err) res.send(err);
        if (deleted) res.send(200);
    });
};

exports.update = (req, res) => {
    Contact.findByIdAndUpdate(req.params.id, req.body, {new: true},  (err, contact) => {
        if (err) res.send(err);
        if (contact) res.json(contact);
    });
};