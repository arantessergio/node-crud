const mongoose = require('mongoose');
const User = require('../models/user');
const jwt = require('jsonwebtoken');
const secretConfig = require('../../config/secret.config');

exports.create = (req, res) => {
    User.create(req.body, (err, user) => {
        if (err) res.send(err);
        if (user) res.json(user);
    });
}

exports.authenticate = (req, res) => {
    User.findOne({email: req.body.email, password: req.body.password}, (err, user) => {
        if (err) res.send(err);
        if (!user) res.send(404);
        if (user) {

            const token = jwt.sign({email: user.email}, secretConfig.secret, {
                expiresIn: '24h'
            });

            user.token = token;

            User.findByIdAndUpdate(user._id, user, {new: true}, (err, updated) => {
                if (err) res.send(err);
                if (updated) res.json(updated);
            })

        }
    });
}