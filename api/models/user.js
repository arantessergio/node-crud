const mongoose = require('mongoose');

const UserSchema = mongoose.Schema({
    email: String,
    password: String,
    token: String
}, {timestamps: true});

module.exports = mongoose.model('User', UserSchema);