const mongoose = require('mongoose');

const ContactSchema = mongoose.Schema({
    name: {
        type: String,
        required: 'The name is required.'
    },
    birth: {
        type: Date,
        required: 'The birth is required.'
    },
    email: {
        type: String,
        required: 'The email is required.'
    },
    phoneNumber: {
        type: String,
        required: 'The phone number is required.'
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('Contact', ContactSchema);