const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const contactRoutes = require('./api/routes/contact');
const userRoutes = require('./api/routes/user');
const morgan = require('morgan');
const jwt = require('jsonwebtoken');
const secretConfig = require('./config/secret.config');
const db = require('./config/database.config.js');

const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('dev'));
app.use(bodyParser.json());

mongoose.connect(db.url);

require('./api/models/contact');
require('./api/models/user');

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Authorization,  Accept');

    const token = req.body.token || req.query.token || req.headers['authorization'];

    if (token) {
        jwt.verify(token, secretConfig.secret, (err, decoded) => {
            if (err) return res.json({success: false, message: 'failed to decode your token'});
            if (decoded) {
                req.decoded = decoded;
                next();
            }
        });
        
    } else if (req.path === '/users' || req.path === '/users/authenticate') {
        next();
    } else {
        return res.json({success: false, message: 'No token provided'});
    }

});

contactRoutes(app);
userRoutes(app);

app.listen(3000, () => console.log('app is running...'));